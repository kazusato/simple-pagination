<?php
require_once('pagination.php');

//現在のページ
$current = (!empty($_GET['p']) && is_numeric($_GET['p'])) ? $_GET['p'] : 1;
//最大のページ数
$max = 30;

//ページャー作成
$pager = pagination($current, $max, 10);
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ページネーション</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
  <div class="pager">
  <? if ($pager['prev']): ?>
    <a href="?p=<?=$current - 1;?>" class="btn btn-default">Prev</a>
  <? endif;?>

  <? foreach($pager['page'] as $v): ?>
    <? if ($current == $v): ?>
    <a href="?p=<?=$v;?>" class="btn btn-default active"><?=$v;?></a>
    <? else :?> 
    <a href="?p=<?=$v;?>" class="btn btn-default"><?=$v;?></a>
    <? endif; ?>
  <? endforeach; ?>
  
  <? if ($pager['omission']): ?>
    <span>...</span>
  <? endif;?>

  <? if ($pager['last']): ?>
    <a href="?p=<?=$max;?>" class="btn btn-default"><?=$max;?></a>
  <? endif;?>

  <? if ($pager['next']): ?>
    <a href="?p=<?=$current + 1;?>" class="btn btn-default">Next</a>
  <? endif;?>

  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

</body>
</html>