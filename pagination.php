<?php
/**
* ページネーション作成
* 
* 現在のページを真ん中あたりでキープするページャー。
* 
* @param  int $current_page 現在のページ
* @param  int $max_page     最大ページ数
* @param  int $show_page    表示するページャーの数
* @return array $pages = [
*                   'prev'     => 1, //"前へ"を表示するか
*                   'next'     => 1, //"次へ"を表示するか
*                   'omission' => 1, //ページ数がたくさんあった時の"..."などを表示するか
*                   'last'     => 1, //"最後のページ"を表示するか
*                   'page' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] //表示するページ
*               ]
*/
function pagination($current_page = 1, $max_page = 1, $show_page = 10)
{
    $current_page = (int)$current_page;
    $max_page     = (int)$max_page;
    $show_page    = (int)$show_page;

    $pages = [];
    //1 => 有効, 0 => 無効
    $pages['prev']     = ($current_page > 1)         ? 1 : 0;
    $pages['next']     = ($current_page < $max_page) ? 1 : 0;
    $pages['omission'] = ($max_page > $show_page)    ? 1 : 0;
    $pages['last']     = ($max_page > $show_page)    ? 1 : 0;

    //表示されるページャー
    $cols = array();
    //ページャーの開始ページ
    $start = 1;
    //ページャーの終了ページ
    $end = ($max_page > $show_page) ? $show_page : $max_page;

    //現在のページを真ん中あたりに表示するための調整
    $adjust = floor((string)$show_page / 2);

    if($max_page > $show_page && $current_page > $adjust)
    {
        //開始、終了予定のページ
        $start_assumed = $current_page - $adjust + 1;
        $end_assumed   = $current_page + $adjust - 1;

        //表示するページャーの数が偶数か奇数で真ん中がずれるため調整
        if($show_page % 2 != 0)
        {
            $start_assumed--;
            $end_assumed++;
        }

        $start = $start_assumed;
        //最大のページを超えるか
        $end = ($max_page > $end_assumed) ? $end_assumed : $max_page;

        //最後のページが表示されているか判定
        if(($start_assumed + $show_page) > $end)
        {
            $start = $end - $show_page + 1;

            //最後のページが表示されていれば不要なので消す
            if($end === $max_page)
            {
                $pages['omission'] = 0;
                $pages['last']     = 0;
            }
        }
    }
    
    for($i = $start; $i <= $end; $i++)
    {
        $cols[] = $i;
    }

    $pages['page'] = $cols;

    return $pages;
}
?>