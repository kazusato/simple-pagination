# PHP Simple Pagination

## Description

だいたい真ん中辺りでカレントページをキープするページネーションをサクッと作れます。

ロジック部分とは切り離してページングに必要なデータのみを返します。

## Getting started

Please copy & paste into your Class or function and view.

Or included.

## Demo

[Simple pagination](http://tk2-207-13211.vs.sakura.ne.jp/demo/pager/)

## Usage

```php
$current = 1;
$max     = 50;
$limit   = 10;

$pager = pagination($current, $max, $limit);

var_dump($pager);
   ↓
array(5) { 
    ["prev"]=> int(0)       //prevを表示するか 0 => no, 1 => yes
    ["next"]=> int(1)       //nextを表示するか 0 => no, 1 => yes
    ["omission"]=> int(1)   //･･･等を表示するか 0 => no, 1 => yes
    ["last"]=> int(1)       //最後のページを表示するか 0 => no, 1 => yes
    ["page"]=> array(10) {  
        [0]=> int(1) 
        [1]=> int(2) 
        [2]=> int(3) 
        [3]=> int(4) 
        [4]=> int(5) 
        [5]=> int(6) 
        [6]=> int(7) 
        [7]=> int(8) 
        [8]=> int(9) 
        [9]=> int(10) 
    } 
}
```

enjoy!